output "arn" {
    description = "ARN bucket"
    value       = aws_s3_bucket.s3_bucket.arn
}

output "name" {
  description   = "Nama (id) bucket"
  value         = aws_s3_bucket.s3_bucket.id
}

output "domain" {
    description = "Domain Name bucket"
    value       = aws_s3_bucket.s3_bucket.website_domain
  
}