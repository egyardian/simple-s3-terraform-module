provider "aws" {
    region = "ap-southeast-1"
  
}

module "website_s3_bucket" {
  source = "./modules/aws-s3"

  bucket_name = "test-egy-ardian-s3-v1"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }

}
#copy file index di dalem modules/aws-s3/ ke directory bucket s3
#aws s3 cp modules/aws-s3/www/ s3://test-egy-ardian-s3-v1/ --recursive