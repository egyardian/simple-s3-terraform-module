variable "bucket_name" {
    type= string
    description = "nama bucket s3"
}

variable "tags" {
  description = "tag yang di set pada bucket"
  type = map(string)
  default = {}
}