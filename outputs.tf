output "website_bucket_arn" {
  description = "ARN Bucket"
  value       = module.website_s3_bucket.arn
}

output "website_bucket_name" {
  description = "nama (id) bucket"
  value       = module.website_s3_bucket.name
}

output "website_bucket_domain" {
  description = "domain name bucket"
  value       = module.website_s3_bucket.domain

}